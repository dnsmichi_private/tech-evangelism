# Events 2020

Date                | CfP Date  | Name              | Location  | Talk      | Details
--------------------|-----------|-------------------|-----------|-----------|----------
30.3.-2.4. 2020     | **Sent**  | KubeCon Europe    | Amsterdam | _no_      | https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/ 
4-6.5.2020          |           | GitHub Satellite  | Paris     | -         | https://githubsatellite.com
15-17.5.2020        |           | DevOps Camp       | Nuremberg | -         | https://devops-camp.de/ - https://devops-camp.de/sponsoringinformationen/
8-10.6.2020         | **Sent**  | Monitorama        | Portland  | From Monitoring to Observability: Migration challenges from Blackbox to Whitebox | https://monitorama.com/2020/pdx.html#cfp https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/1593 
17.-19.6.2020       | 1.4.2020  | DevOpsDays        | Amsterdam | **TODO**  | https://cfp.devopsdays.amsterdam/2020/cfp - **propose workshops: GitLab CI, or another CI alike topic. 2.5h.**
22-24.6.2020        | **Sent**  | ContainerDays     | Hamburg   | From Monitoring to Observability: Cloud Native Challenges from Blackbox to Whitebox | https://sessionize.com/containerdays-2020/
23-25.6. 2020       | **Sent**  | IcingaConf        | Amsterdam | From Monitoring to Observability: Challenges in a Cloud Native World | https://icingaconf.com/ 
13-16.7.2020        | **Sent**  | O'Reilly OSCON    | Portland  | _no_      | https://conferences.oreilly.com/oscon/oscon-or/public/cfp/781 https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/1511 
16-17.7.2020        | ???       | PromCon NA        | Vancouver | **TODO**  | https://promcon.io/2020-vancouver/ 
21-24.9. 2020       | ???       | DevOps World      | Las Vegas | **TODO**  | https://www.cloudbees.com/devops-world 
1.10.2020           | ???       | DevOpsDays        | Ankara    | **TODO**  |  https://twitter.com/srhtcn/status/1229394439157768193?s=21 
19-21.10. 2020      | 15.5.2020 | OSAD              | Munich    | **TODO**  | https://osad-munich.org/ - https://osad-munich.org/call-for-papers/ (workshop possible, 5-6h)
21-23.10.2020       | 1.7.2010  | DevOpsDays        | Eindhoven | **TODO**  | https://devopsdays.org/events/2020-eindhoven/welcome/ 
3-6.11.2020         | 4.5.2020  | ContainerConf     | Mannheim  | **TODO**  |  https://www.containerconf.de/call_for_proposals_en.php?source=
12-15.11. 2020      | 19.5.2020 | Continous Lifecycle | Mannheim | **TODO** | https://www.continuouslifecycle.de/call_for_proposals.php?source=0 
16-19.11. 2020      | May 2020  | Infra & Ops O'Reilly EU | Berlin | **TODO** | https://conferences.oreilly.com/infrastructure-ops/io-eu
16-19.11. 2020      | May 2020  | OSMC              | Nuremberg | **TODO** | https://osmc.de 
Nov 2020            | ???       | PromCon EU        | ???       | **TODO** | https://promcon.io 
30.11.-3.12. 2020   | ???       | DevOpsCon         | Munich    | **TODO** | https://devopscon.io/munich/ 
