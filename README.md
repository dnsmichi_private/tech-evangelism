# Tech Evangelism

1. [Introduction](#introduction)
    - 1.1 [Definition](#definition)
    - 1.2 [Measurement](#measurement)
2. [Ideas List](#ideas-list)
    - 2.1 [Events](#events)
    - 2.2 [Talks](#talks)
    - 2.3 [Demos](#demos)
    - 2.4 [Research](#research)
    - 2.5 [Feedback Loop](#feedback-loop)
3. [Topics](#topics)
    - 3.1 [CI/CD](#topic-cicd)
    - 3.2 [Cloud](#topic-cloud)
    - 3.3 [Monitoring & Observability](#topic-monitoring-observability)
    - 3.4 [Development (Workflows, Serverless)](#topic-development)
    - 3.5 [Integrations](#topic-integrations)
4. [Getting Started](#getting-started)
    - 4.1 [Documentation](#documentation)
    - 4.2 [Trainings](#trainings)
5. [Knowledge and Evangelism](#knowledge-and-evangelism)
    - 5.1 [People and Media](#people-and-media)
    - 5.2 [Background](#background)
    - 5.3 [Evangelism](#evangelism)
    - 5.4 [Regions](#regions)
    - 5.5 [Community Building](#community-building)
6. [History](#history)
    - 6.1 [Articles & Blogposts](#articles-blogposts)
    - 6.2 [Book Reviews](#book-reviews)

<a id="introduction"></a>
## Introduction

This project provides ideas, notes, stories from myself for diving deep into technical evangelism.

Following GitLab's [values](https://about.gitlab.com/handbook/values/), everything is developed in the open.

If you think that something is missing, or wrong, please create a merge request or kindly contact me via michael.friedrich@gmail.com

<a id="introduction-definition"></a>
### Definition

Tech Evangelism: https://youtu.be/DX5xFLnHMuc

”Evangelism is an individual opinion and point of view as opposed to some kind of manufactural message in marketing.”

<a id="introduction-measurement"></a>
### Measurement

How do we define results and measure our performance? 

- Contribute to OSS, being listed/thanked by release blog posts. Or highlighted with a technical blog post and getting an audience enabled from others.
- Be a role model for others who start giving talks about GitLab, CI, etc. and put you into context. Build a community of heroes and evangelists, enable them for talks, demos, presentations. 
- Reach on tweets and social activities

Enable community evangelists:

- Feu - https://twitter.com/the_FeuFeu/status/1221709718089011200
- Mario - https://twitter.com/m4r10k/status/1220956270317985793 (hero)
- Marcel - https://twitter.com/winem_/status/1218096802060275713 


<a id="ideas-list"></a>
## Ideas List

- Build blog posts and articles which users easily adopt and spread the love. Visual appearance works better on social media. Use fun demos e.g. with emojis to push sharing.
    - CI/CD landscape or security in CI/CD, as article follow-up to https://www.heise.de/select/ix/2019/4/1553943972989985 
- Security in CI pipelines and in between, explain this in blog series and push more towards infosec groups
- Join development (language) communities, like C++: https://twitter.com/pati_gallardo/status/1196008365115265024?s=21
    - [Workflows](https://forum.gitlab.com/t/gitlab-ci-cd-pipeline/32773/2), Tests, Documentation, [good Changelog entries](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/37053), project visibility
- CI/CD monitoring, dashboards, developer visibility without shell access
- CI/CD for CNCF projects
    - Prometheus needs RPM/DEB packages to get people started more easily: https://github.com/prometheus/prometheus#install Discussion started: https://twitter.com/juliusvolz/status/1204039907175481345 
    - Help exporter developers with CI setups, e.g. ssl_exporter: https://github.com/ribbybibby/ssl_exporter/pull/16 
- CICD for Automation projects
    - Library, module tests, environments
- Learn how to create a distribution package registry, similar to GitHub. https://about.gitlab.com/direction/package/package_registry/ 
    - This would also enable GitLab itself to e.g. create CentOS 8 packages and uploads, moving away from external hosting.
    - Follow JFrog/Artifactory: https://jfrog.com/open-source/ 
- Make it easier to try GitLab e.g. in regard of CI/CD and/or Kubernetes. Share code snippets on gitlab.com  
- Enable GitLab heroes and influencers with technical material, training classes, presentation demos - create a structured project for them and anyone else at GitLab.  

- Articles
    - Puppet Forge Redesign, https://forge.puppet.com/configuration-management/puppetlabs/deploy-splunk-enterprise-in-minutes
        - Add puppet-gitlab
    - Open Souce Contributions Stories
        - https://upshotstories.com/stories/why-i-contribute-to-the-open-source-community-and-you-should-too
        - https://www.bwplotka.dev/2020/how-to-became-oss-maintainer/ 

<a id="ideas-list-events"></a>
### Events

**2020**: Moved to [events_2020.md](events_2020.md).



2021:

Date                | CfP Date  | Name                                      | Details
--------------------|-----------|-------------------------------------------|-----------------------------
Feb 2021            | ???       | FOSDEM                                    | https://fosdem.org/2020/
Feb 2021            | ???       | CfgMgmtCamp                               | https://cfgmgmtcamp.eu/ 
Mar 2021            | ???       | KubeCon AMS                               | TBD 
Mar 2021            | ???       | Git Merge LA                              | https://git-merge.com/ 
???                 | ???       | DeliveryConf EU                           | I'm in touch with the organizers. https://twitter.com/divineops/status/1196891287619821571?s=21
May 2021            | ???       | Continuous Lifecycle London               | https://continuouslifecycle.london/
June 2021           | ???       | DevOpsCon Berlin                          | https://devopscon.io/berlin/ 


#### Meetups

Coming up:

- 21.4.2020 London GitLab Meetup: https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/824 

##### Nuremberg

- Kubernetes Nuremberg (CNCF Group): https://www.meetup.com/Kubernetes-Nurnberg/ 
- DevOps Nuremberg: https://www.meetup.com/devops-nuernberg/ 
- Hackerkegeln by Datev: https://hackerkegeln.de/ 

##### Germany

- Prometheus Berlin: https://www.meetup.com/Berlin-Prometheus-Meetup/ 
- GitLab Meetup Hamburg: https://www.meetup.com/GitLab-Meetup-Hamburg/ 

##### Austria

- DevOps & Security Vienna: https://www.meetup.com/Vienna-DevOps-Security/ 
- Icinga Users Austra: https://www.meetup.com/Icinga-Users-Austria/ 
- Icinga Users Berlin: https://www.meetup.com/Icinga-Users/

##### Europe

- Cloud Native Prague: https://www.meetup.com/Cloud-Native-Prague/ 
- GitLab Meetup London: https://www.meetup.com/London-Gitlab-Meetup-Group/
- Linuxing in London: https://www.meetup.com/Linuxing-In-London/ 
- Metrics and Monitoring London: https://www.meetup.com/London-Metrics-and-Monitoring/ 


<a id="ideas-list-talks"></a>
### Talks

Ideas for own talks. Avoid product pitches, or those which may look like that. Provide a mix of technologies and educate people.

#### CI/CD

- Git, Containers, CI: How I became a better Open Source Developer
    - History of trunk based development with CSV, SVN, Git
    - Works on my machine vs VirtualBox with manual RHEL/SLES installation
    - Hudson CI, my Windows builds are broken
    - Now we have Jenkins but no-one looks at the notifications
    - Pull Requests with integrated CI making reviews easier
    - Packages for the win - Docker containers, registry and GitLab CI magic
    - Kubernetes or how I can test my feature branch live in a production environment
    - Distributed clusters as Terraform Deployment - git bisect broken builds in the wild
    - Multicloud vs embedded hardware development, where to?
    - Web IDEs and remote collaboration

- As a developer, I don't care about Kubernetes
    - git clone a repo, develop stuff, push things
    - Check CI results, Check the deployed apps
    - Background checks and debugging k8s? Have ops people for that ;)
    - add GitLab Auto-DevOps capabilities
    - explain why devs should care and how they can control the configs to make their lives even easier
    - Grafana dashboards from k8s metrics, for developers to see their cluster deployments

- You don't need to be a YAML developer to become a CI/CD super hero
    - Overview of CI/CD tools
    - Oh, there's a yaml
    - Templates, generators, automation
    - Abstraction layers for containers builds, registry, packages, deployments, auto devops, k8s, Terraform
    - Yaml syntax error. Fix your infrastructure as code.
    - Learn: Everything is a DNS problem.

- CI/CD Security: Is this a thing?
    - GitHub action runners with config modified in forks can run arbitrary code in your environment
    - TLS, tokens, exploits for Docker containers
    - Security aint easy, CI is already complicated
    - Not only our pipeline communicaation needs security, our applications too
    - Follow along the Zero Trust principle

#### Monitoring 

- From Monitoring to Observability: Migration challenges from Blackbox to Whitebox
    - Inventory, edit config by hand, use Infrastructure as code (Puppet, Chef, Ansible, etc.)
    - Classic Icinga services with metrics - easy
    - MySQL database monitoring
    - VCenter sends SNMP traps, can Prometheus handle that for me?
    - I didn't monitor Kubernetes yet, what's in there for me?
    - How about distributed tracing with Jaeger?
    - Can I replace Elastic Stack with Grafana Loki?    
    - Alerts, downtimes, SLA reporting - change is hard for admins
    - Dashboards - Grafana and alternatives
    - Writing a simple plugin vs a node exporter
    - Can't we just combine the two?

- Monitoring challenges in a cloud native world
    - Nagios, Icinga - host and service, Apply that to Vmware/Xen
    - Move to single APIs checking the overall state
    - I'm running in a container now, can I install a second service as agent? Sidecare checks needed, query k8s API, Docker Inspect, etc.
    - Blackbox vs Whitebox monitoring vs observability
    - Metric streams - graphs tell stories, correlate logs, events, metrics, alerts
    - How Prometheus, InfluxDB's TICK stack and Grafana come to play
    - Can we still use business process modelling and metric dashboards?
    - end2end monitoring in its classic way - superseeded with OpenTracing, or data kraken like Meltano?
    - Practical examples
        - docker-compose LAMP stack with load balanced nginx containers and HAproxy - what's wrong, what's right?
        - Prometheus Exporter

- From Monitoring to Observability: The Sequel
    - Plugins, Node Exporters, APIs - the admin's pain
    - Honeypot, where are my events
    - Store events as metric, log, whatever. Rehi MQTT and IoT
    - Scale out in NoSQL. Rehi Java.
    - Ready to use event proxies for DMZs
    - TLS by default and with auto-signing mechanisms. Rehi Puppet.
    - Machine learning dashboards and alerting rules
    - Skynet is calling.


<a id="ideas-list-demos"></a>
### Demos

- Funny web demos with animated GitLab logo, [Clippy](https://github.com/Icinga/icinga2-api-examples/tree/master/clippy.js), etc. e.g. after provisioning a container app
    - Involve ChartJS, Grafana widgets, etc. 
    - Tanuki Logo animation: https://gitlab.com/dnsmichi/animated-tanuki 
- Vagrant Environments
    - Create a GitLab playground with CI/CD jobs automatically provisioned, and add monitoring checks. 
    - Combine Prometheus with Icinga:  https://github.com/Icinga/icinga-vagrant/pull/200
- Cover more distributions/containers
    - RHEL8 with app streams are new, how does this affect developers with their repository dependencies and EPEL
    - Windows and macOS builds
    - Package/container registry
- Integrations
    - Provide an Icinga Web module which incorporates GitLab CI pipeline status, health, tickets, MRs into monitoring. Allow to create new issues from monitoring incidents. Similar to https://icinga.com/docs/jira/latest/doc/01-Introduction/     

<a id="ideas-list-research"></a>
### Research

- Follow https://twitter.com/search?q=gitlab&f=live in TweetDeck.
- Twitter lists:
    - https://twitter.com/dnsmichi/lists/development
    - https://twitter.com/dnsmichi/lists/devops-culture
    - https://twitter.com/dnsmichi/lists/gitlabbers 

#### Talks

Talks I consider valuable for my journey, learning everyday.

- CNCF.CI at KubeCon 2019 https://youtu.be/OFSBRn-PWNI
- Tekton and GKE https://youtu.be/TQJ_pdTxZr0
- Large Scale CD with Spinnaker at Netflix https://youtu.be/PLNheBiWOGI
- k8s, Spinnaker, istio for multicloud https://youtu.be/kKC-VgAptII
- k8s The easy way https://youtu.be/kOa_llowQ1c
- Kubernetes explained https://www.youtube.com/watch?v=aSrqRSk43lY 
- Istio Service Mesh https://www.youtube.com/watch?v=6zDrLvpfCK4 
- CD with Spinnaker and OpenStack https://youtu.be/CISaOduHNuY
- Automating k8s deployments (GitLab) https://youtu.be/wEDRfAz6_Uw
- Ansible and GitLab for Infrastructure as code https://youtu.be/M-SgRTKSeOg

Ideas for CI/CD integration

- KubeVirt https://youtu.be/_z5Pjyl0Dq4
- KubeDirector https://youtu.be/X2kEk5wLe9g

#### CICD

Collected blog posts and articles which propose good examples to start with.

- A Microservices Workflow with Golang and Gitlab CI + k8s https://imti.co/gitlabci-golang-microservices/ 
- Lessons learned with Gitlab Runner on Kubernetes https://medium.com/90seconds/lessons-learned-with-gitlab-runner-on-kubernetes-d547c30ad5fb 
- From Dev to Prod with GitLab CI (PHP UK) http://trumpathon.com/from-dev-to-prod-with-gitlab-ci/ 
- Build optimization mechanism with GitLab, Gradle, Docker https://www.grammarly.com/blog/engineering/build-optimization-mechanisms-gitlab-gradle-docker/ 
- GitLab with Docker and Traefik https://www.davd.io/byecloud-gitlab-with-docker-and-traefik/
- Using GitLab CI/CD to auto-deploy your Vue.js Application https://morioh.com/p/58da8febce9c 
- Group conversation 28.11.2019 https://youtu.be/_XHGUtWoOv4
- Testing your RedTeam Infrastructure https://blog.xpnsec.com/testing-redteam-infra/ (CI, inSpec, Mocule)

#### Security

- https://blog.runpanther.io/open-source-cloud-security-tools/ 

<a id="ideas-list-feedback-loop"></a>
### Feedback Loop

- Chime into questions on Twitter, search tab in Tweetdeck
- Learn from users in the GitLab community
    - https://forum.gitlab.com/u/dnsmichi/activity
- Collect ideas from the community and help product managers. Also, care about sales and provide them with basic pitches, especially when comparison is needed.
- Discuss and offer help
    - Teach and share the training/workshop offers from NETWAYS (CC non-commercial license) https://github.com/NETWAYS/workshop-osmc-gitlab


#### Feedback: Ongoing

Anywhere where I have joined the conversation already or people are waiting for my feedback.

##### CI/CD

- [x] **Discuss**. Get up and running quickly with GitLab CI - https://gitlab.com/groups/gitlab-org/-/epics/2072#note_235718368
    - collected ideas in a chat with @jlenny in CW49 - future PM needed, let's see what 2020 brings
- [x] **Discuss**. Blackout windows for CI deployments.
    - [x] Chimed into the discussion and added [my thoughts](https://gitlab.com/gitlab-org/gitlab/issues/39108#note_272002791). Connected @jmeshell with @kencjohnston :)

###### Releases   

- [x] **Watch**. Release Management User Experience
    - [x] Had a chat in CW03 with @jmeshell about release generation, binary assets, and even more topics
    - [x] Chat in CW04 with @jmeshell and @rverissimo for the release UX parts
- [x] **Watch**. [RO: Make Releases page complete](https://gitlab.com/groups/gitlab-org/-/epics/2131)
    - [x] Coming from CW03 chat with @jmeshell - added [my feedback](https://gitlab.com/groups/gitlab-org/-/epics/2131#note_271995029)
- [x] **Watch**. Releases from CI config - https://gitlab.com/gitlab-org/gitlab/issues/26013#note_253368713 https://forum.gitlab.com/t/gitlab-ci-artifact-promotion/32133 
    - Connecting ideas with the package registry: https://gitlab.com/gitlab-org/gitlab/issues/26013#note_253421170
    - Had a chat in CW51 with @trizzi about RPM/DEB package repositories, enterprise features like mirroring, etc.
- [x] **Discuss**. Validation: Publish releases in Maintainer's automated workflow https://gitlab.com/gitlab-org/gitlab/issues/38037
    - [x] Added my feedback: https://gitlab.com/gitlab-org/gitlab/issues/38037#note_267273526
- [x] **Watch**. Release evidence - test results, and MR reviews.
    - [x] Shared ideas with @jmeshell https://gitlab.com/gitlab-org/gitlab/issues/32773#note_273089686 

##### Monitor

- [x] **Watch** Monitor & APM
    - [x] Watched the discussion: https://youtu.be/aDfUaDqrh60
    - [x] Asked for epics: https://twitter.com/dnsmichi/status/1212896402201874433
    - [x] Epics: [Add more dashboard chart types](https://gitlab.com/groups/gitlab-org/-/epics/1897) & [APM: Infrastructure Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2135) - [My feedback](https://gitlab.com/groups/gitlab-org/-/epics/1897#note_267291038)
    - [ ] Waiting for @kencjohnston to coordinate a meeting

##### Community

- [x] **Watch** Forum topic: [Open Source vs Community Building - get better](https://forum.gitlab.com/t/is-this-product-open-source-no-charge-i-host-from-home/32225/3) - 
    - [x] Joined the conversation with @LindsayOlson and @greg via Forum PM
    - [x] New Issue created: [Ideas for community building at the GitLab Discourse forum](https://gitlab.com/gitlab-com/marketing/community-relations/general/issues/8)
        - [x] [Dashboard](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues/69)
        - [x] [Revise Categories](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues/70)

##### Training

- [ ] Training courses and gamification
    - [ ] **Waiting for feedback** Connection from @sytses to @emilie for GitLab feature class, https://twitter.com/emilieschario/status/1199478933894418433 
    - [ ] **Watch** https://gitlab.com/gitlab-org/growth/product/issues/107#note_281696158
    - [ ] That system could also prevent spam, like Discourse: https://gitlab.com/gitlab-org/gitlab/issues/14156#note_258252735 

#### Feedback: Pending

- [ ] Todos & notifications https://gitlab.com/groups/gitlab-org/-/epics/1750 
- [ ] Product Management Insights
    - [ ] Value vs. Cost https://gitlab.com/gitlab-org/gitlab/issues/202041 - add thoughts for @markpundsack 
    - [ ] Consider this for OSS maintainers too - community demands


#### Feedback: Backlog 

- [ ] Log aggregation - https://gitlab.com/gitlab-org/gitlab/issues/194017
- [ ] Recurring Issues - https://gitlab.com/gitlab-org/gitlab/issues/15981
  - Share the story from the NETWAYS marketing team looking for an alternative to RT
- [ ] Audit log events for git fetch/clone/pull - https://gitlab.com/gitlab-org/gitlab/issues/10771 https://gitlab.com/groups/gitlab-org/-/epics/640 
- [ ] Support for DNS authorization with Let's Encrypt - https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3209 
- [ ] Windows images, base 1903 - https://gitlab.com/gitlab-org/gitlab-runner/issues/4396
- [ ] Dynamic job names from variables - https://gitlab.com/gitlab-org/gitlab/issues/28734#note_253261094 (harder than I thought, needs scoping and more advanced lexer knowledge, https://gitlab.com/gitlab-org/gitlab-runner/issues/1809#note_225636231)



#### Feedback: Done

- Community 
    - [x] Create issues from commits or commit messages - https://twitter.com/olearycrew/status/1199330526668173314 
        - Extend quick actions: https://gitlab.com/help/user/project/quick_actions
    - [x] GitLab pages DNS verification - https://forum.gitlab.com/t/gitlab-pages-dns-txt-propagation/32102/4?u=dnsmichi
    - [x] Discuss the need of issue templates with Linux distributions: https://twitter.com/dnsmichi/status/1201858307566252032 
- [x] Writing docs: "gitlab markdown engine fails to work with html anchors" - https://gitlab.com/gitlab-com/support-forum/issues/4211#note_250539911 

#### Possible Feature Requests

TODO list for myself to create detailed GitLab issues.

- To-Do list feature
    - Allow to add notes, labels, organize them. Replace Microsoft To-Do/Wunderlist, Evernote, etc. with reminders also showing up
    - Maybe explore a whole new feature area, outside of GitLab issues and project management
- Inspect running containers (CI, k8s, cloud) from the Web IDE. Inspired by VS Code extension: https://code.visualstudio.com/blogs/2019/10/31/inspecting-containers 
- Evaluate [GitPod](https://www.gitpod.io/features/) and open according feature requests for the Web IDE


<a id="topics"></a>
## Topics

<a id="topics-cicd"></a>
### Topic: CI/CD

Continuous Delivery Foundation: https://cd.foundation
Wait for FOSDEM2020 recordings, https://fosdem.org/2020/schedule/track/continuous_integration_and_continuous_deployment/

https://twitter.com/dnsmichi/status/1223929201901940737?s=21

#### Ideas

- Feedback loop with developers and product managers
    - Started this with https://gitlab.com/groups/gitlab-org/-/epics/2072 

- Push the Web IDE
    - Demo vids, clips showing how developers can profit
    - Compare against other IDEs
    - Explain the workflows with CI/CD

- Emphasize on developer communities
    - CI doesn’t work without unit tests. Take care and be a role model.
    - Make it easier to build packages throughout the pipelines, including dependency management
    - Share best practices on rpm/deb packaging for better CI pipelines
    - Static code analysis & security explained (subtle EE marketing)

- Include the admins
    - Ensure that CI is easy to try
    - Use the simple exercises from training slides
    - Enable them to use Docker and build images.
    - Allow them to understand which CI components their developers need.

- Improve on Docker-in-Docker workflows to build images.
    - With the recent sell-out on Docker Inc, the future of dockerhub is unclear.
    - We must enable GitLab users to easily build their images and use the built-in registry better.
    - Enabling the registry by default already is in progress: https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4690 
    - Evaluate how others build images, e.g. https://getintodevops.com/blog/the-simple-way-to-run-docker-in-docker-for-ci 
    - Evaluate how to provide multi version language image templates (in one Docker image or many)

- Automation, enable users to quick start their GitLab experience
    - Contribute to Puppet/Ansible/Chef/Salt modules to enable full CI/CD usage
    - https://forge.puppet.com/puppet/gitlab 
    - https://docs.ansible.com/ansible/latest/search.html?q=gitlab&check_keywords=yes&area=default
    - _Note:_ Puppet is my expertise, I have basic knowledge about Ansible, Chef, Salt and updating their modules. I'm familiar with rspec tests, and also Python and Ruby in general.

- Auto DevOps and alike
    - Cloud native build packs https://gitlab.com/gitlab-org/gitlab/issues/25954 

- Test/implement bleeding edge alpha features and provide feedback
    - Matrix builds proposal
    - DAG

- Documentation
    - Each project might somehow be complicated to start with, or some things might be unclear.
    - Let them know, send patches even
    - Documentation needs more love. And there's also two sides - the user who needs to get started easily, and the developer describing the feature set.
        - Documentation can be improved from community support
        - Encourage developers to follow best practices with feature documentation


#### SIGs

- CICD SIGs
    - https://github.com/cdfoundation/sig-interoperability
    - https://github.com/cdfoundation/sig-mlops
    - https://github.com/cdfoundation/sig-security


- Kubernetes SIGs & Community
    - https://discuss.kubernetes.io/categories 
    - https://github.com/kubernetes-sigs/sig-windows-tools 
    - https://github.com/kubernetes-sigs/k8s-container-image-promoter 
    - https://github.com/kubernetes-sigs/sig-storage-lib-external-provisioner 
    - https://github.com/kubernetes-sigs/sig-usability 
    - https://github.com/kubernetes-sigs/node-feature-discovery 
    - https://github.com/kubernetes-sigs/cluster-api-provider-gcp 
    - https://github.com/kubernetes-sigs/cloud-provider-azure 

#### Workflows 

 - GitOps
    - See how Terraform comes into play here. At GitLab Commit London everyone used Terraform for deployments.  
        - https://about.gitlab.com/blog/2019/11/12/gitops-part-2/ 
        - https://gitlab.com/gitlab-org/gitlab/issues/32821
    - Puppet's Lyra project, cloud native workflows, got cancelled. Still, the tools are available: https://github.com/lyraproj/hiera (Hiera written in Go)        


#### Projects

https://cd.foundation/projects/ 

#### Jenkins

Jenkins is all about plugins, and it is hard to tackle to find the right plugins. Also, sometimes key plugins are unmaintained, or break the upgrades without fixes.

Deploying Jenkins by hand is difficult, with the use of Puppet this can be made easier.

- Contribute to plugins and documentation
- Help maintain the GitLab integration at https://wiki.jenkins.io/display/JENKINS/GitLab+Plugin 
- Work with Puppet/Ansible modules to improve the deployment
- Come with a strong focus on security, and harden their interfaces.
    - Building things on the master without agents - terrible idea. 
    - https://jenkins.io/blog/2019/10/21/thinking-about-jenkins-security/ 
- Evaluate DSL plugin, consider migrations to GitLab config: https://github.com/jenkinsci/job-dsl-plugin
- Code contributions. _Note:_ I learned Java in 2007, but I wouldn't call myself a Java developer. I understand Java code, and can improve my skills on this again.


#### Jenkins X

Either runs Jenkins from a Jenkinsfile in a JVM container in kubernetes, or uses Tekton pipelines with Prow ([source](https://www.inovex.de/blog/spinnaker-vs-argo-cd-vs-tekton-vs-jenkins-x/)). 

I've tried it once, but found the [getting started](https://jenkins-x.io/docs/getting-started/) documentation hard to follow. It doesn't explain the key principles on requirements and also, it doesn't follow the "one way for the first success" method. This would be my first one to actually contribute and make it easier to start with the project.

Their [contributing docs](https://jenkins-x.io/docs/contributing/) also highlight Windows, where I could share my expertise. I have a Windows 10 license for my private projects.

> Moritz Plassnig on [Twitter](https://twitter.com/moritzplassnig/status/1216558708223856640?s=12):
>
> Btw. JX uses Tekton under the hood as the pipeline execution engine

- https://jenkins-x.io/blog/2019/02/19/jenkins-x-next-gen-pipeline-engine/
- https://technologists.dev/posts/tekton-jx-pipelines/
- https://medium.com/@mahendra.yadavali/jenkins-x-with-tekton-pipelines-on-kubernetes-5ea796b356c2 


#### Spinnaker

Issues: https://gitlab.com/gitlab-org/gitlab/issues/35219

This doesn't replace CI, it requires a "build server". Currently they aim to use Jenkins or Travis CI.

There's a GitLab integration going on, and jumping in there to help would be a good idea: https://github.com/spinnaker/spinnaker/issues/2047

The initial "go" needs cloud resources, local minikube is not an option. This might be an option to contribute. 

https://www.inovex.de/blog/spinnaker-vs-argo-cd-vs-tekton-vs-jenkins-x/ 

#### Tekton

Introduction: https://www.youtube.com/watch?v=TWxKD9dLpmk 

Written in Go. RedHat's OpenShift pipelines use this in future versions: https://blog.openshift.com/pipelines_with_tekton/
Introduction into OpenShift: https://www.youtube.com/watch?v=KTN_QBuDplo Tekton seems to become a number one competitor to Auto DevOps.

Tasks:

- Figure out how GitLab CI/CD can be integrated, open issue: https://github.com/tektoncd/pipeline/issues/1000 
- Search for APIs and create client or library code. 
- With OpenShift also moving away from Jenkins, see how the technology works in regard of GitLab. https://blog.openshift.com/cloud-native-ci-cd-with-openshift-pipelines/ 
- Since OpenShift doesn't use Docker anymore, but podman, etc. also look for possibilities to do the same with the GitLab runners. 

https://www.inovex.de/blog/spinnaker-vs-argo-cd-vs-tekton-vs-jenkins-x/

> The Tekton maintainers plan to implement a long list of new features before the end of this year. This includes conditional execution, cancelling or pausing a workflow, resuming a paused or failed workflow and enforcing timeouts on Tasks and Pipelines.

See whether specific features, tests, QA, docs are applicable.

https://developers.redhat.com/blog/2019/07/22/how-to-build-cloud-native-ci-cd-pipelines-with-tekton-on-kubernetes/
https://developer.ibm.com/tutorials/knative-build-app-development-with-tekton/

#### Argo CD

https://argoproj.github.io/argo-cd/

> Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.

Jenkins integration: https://stuarthowlette.me.uk/posts/argocd-jenkins-pipeline/

- Can use SSO and webhooks from GitLab
- Channel the results back to GitLab? (seems to be missing)

#### Concourse

https://concourse-ci.org/ - evaluate it. 

#### Drone.io

Has a GitLab adapter for triggering pipelines: https://blog.drone.io/drone-adapter-gitlab-pipelines/
They also have a .gitlab-ci.yml converter CLI tool.

#### Gitea

Does not have CI/CD, but lists many other tools. As it is written in Go, look for a possible GitLab CI integration.

https://docs.gitea.io/en-us/ci-cd/

#### Project Nebula

Deployment workflows by Puppet. Announced at Puppetize PDX in October, had a chat about that at DevOpsDays Ghent.

https://puppet.com/project-nebula
https://youtu.be/asXiXwHyqwY

Uses GH/Bitbucket as Git storage, push to extend this. Signed up for their Beta.

#### GitHub Actions

I've been in their beta program since December 2018, and have provided in-depth feedback on getting started and using the actions. With the gentle guidelines and inline help, I can see that some of my feedback reached the final product. Also, the previous interface was more Docker-like, and not end-user friendly. With their CI/CD addon on top, this exactly hides what I critized the most.

- Continue with testing software development on GitHub, and evaluate possibilities
    - icinga2 is written in C++, a PR exists: https://github.com/Icinga/icinga2/pull/7592 
    - dashing-icinga2 is written in Ruby: https://github.com/dnsmichi/dashing-icinga2 
- Feedback loop to engineering for adding features to GitLab CI/CD
    - Partially done in https://gitlab.com/groups/gitlab-org/-/epics/2072
- Create a GitHub action which integrates GitLab CI via API (trigger CI, feedback into GH caller)
    - https://github.com/marketplace/actions/trigger-gitlab-ci 

#### Travis CI

With the recent sold-out and setting free many developers, it is unclear where they are heading. Also, their platform needs an update to Ubuntu 18 Bionic (currently 16 Xenial).
Their build matrix and also the possibilities to test different languages in chroots is something where GitLab should aim for the long term. With GH Actions now available, many users will move their CI/CD train from Travis. With following the multi language idea, this could attract them to GitLab. Technical blog posts a like.


#### JetBrains TeamCity

Only the plugins are on GitHub, also proprietary license. Only evaluate new features from time to time for answering questions in talks or meetups.

#### JFrog Artifactory

Integrates the CI builds e.g. with Jenkins. GitLab is not supported yet.
https://www.jfrog.com/confluence/display/RTF/Build+Integration 
https://www.jfrog.com/confluence/display/RTF/Jenkins+Artifactory+Plug-in 
https://github.com/jfrog/jenkins-artifactory-plugin 

Relevant for learning and pushing the product: https://about.gitlab.com/direction/package/package_registry/ 
Example guide from GitLab: https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html 


#### Dokku

http://dokku.viewdocs.io/dokku/ similar to Heroku apps, PaaS. Can be integrated with GitLab CI: http://dokku.viewdocs.io/dokku/community/tutorials/deploying-with-gitlab-ci/ 

Competitor to GitLab pages? Or maybe, we can adopt ideas from it for GitLab web apps as a product idea.


<a id="topics-cloud"></a>
### Topic: Cloud

#### Envoy Proxy

Written in C++, similar patterns to Icinga 2.
https://github.com/envoyproxy/envoy

#### Traefik

https://containo.us/traefik/ 
https://www.netways.de/blog/2019/02/21/gitlab-ci-docker-traefik-%E2%9D%A4%EF%B8%8F/


#### Crossplane

From #multicloudcon tweets: https://github.com/crossplaneio/crossplane/releases/tag/v0.5.0

https://crossplane.io/docs/v0.5/workflow/gitlab.html
https://docs.gitlab.com/ee/user/clusters/applications.html#crossplane

#### koris

Spin up a k8s cluster in Openstack in minutes. New project by a big provider with multiple data centers in Germany.

https://gitlab.com/noris-network/koris/ 


<a id="topics-monitoring-observability"></a>
### Topic: Monitoring & Observability

**Task list**: [monitoring_observability.md](monitoring_observability.md) including Prometheus

#### Ideas 

Share ideas with the product teams:

- Dashboards
    - Keep using the feedback from Dashing for Icinga, private project: https://twitter.com/Mikeschova/status/1229415489627181056 
- TLS monitoring: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34943
    - Icinga module: https://icinga.com/products/icinga-certificate-monitoring/
    - Icinga cluster with CSR auto-signing, etc.: https://icinga.com/docs/icinga2/latest/doc/19-technical-concepts/#cluster 
    - Prometheus ssl_exporter and blackbox_exporter projects - not so easy to configure/deploy
    - Prometheus Exporter Security with TLS
- Product comparison: https://about.gitlab.com/devops-tools/nagios-vs-gitlab.html 
    - Service and Hardware monitoring    
- Cloud & container monitoring
    - Auto-discovery
    - Cloud dashboards, like Crossplane
- Implement OpenTracing/Jaeger support
    - C++ for Icinga https://github.com/opentracing/opentracing-cpp https://github.com/jaegertracing/jaeger-client-cpp (keep the Apache license in mind, incompatible woth GPLv2, therefore no binary shipping in debug builds)
    - Go 
- Log aggregation and event pipelines


#### Grafana Dashboards

With observability moving into core, and dashboard ideas for GitLab, we can also look into Grafana dashboards e.g. for CI/CD, k8s, etc.

- Auto-provisioning the Grafana dashboards via REST API needs some love, e.g. wrapping the payload and replacing all variables. Using a workaround inside Vagrant, https://github.com/grafana/grafana/issues/2816#issuecomment-364728953  _Might be a candidate for contributions_

#### Jaeger Tracing

Look into CNCF projects and verify whether they need support for OpenTracing/Jaeger.

- Started with Icinga and the basics: https://www.netways.de/blog/2020/01/09/from-monitoring-to-observability-distributed-tracing-with-jaeger/ 

#### Falco 

- Falco [graduated](https://thenewstack.io/cncfs-falco-runtime-security-tool-graduates-from-the-sandbox-moves-into-incubation/) to CNCF incubated
    - https://falco.org/ "Falco, the open source cloud-native runtime security project, is the defacto Kubernetes threat detection engine. Falco detects unexpected application behavior and alerts on threats at runtime."
    - Try it: https://falco.org/docs/installation/ 


#### Honeycomb

Evaluate their observability stack, test APIs and integrations and gather insights for talks and technical deep dives.
- https://www.honeycomb.io/overview/ 
- https://www.honeycomb.io/serverless/
- https://www.honeycomb.io/microservices/ 


<a id="topics-development"></a>
### Topic: Development

#### Serverless

- [AWS Lambda](https://aws.amazon.com/lambda/)
    - https://docs.aws.amazon.com/lambda/latest/dg/lambda-python.html 


<a id="topics-integrations-api"></a>
### Topic: Integrations

- Drupal GitLab API: https://www.drupal.org/project/gitlab_api 
- Repository management with Pulp: https://pulpproject.org/2019/12/12/pulp-3-is-GA/ 
    - RPM, Container, etc. 
    - Pulp is part of RedHat Satellite


<a id="getting-started"></a>
## Getting Started

<a id="getting-started-documentation"></a>
### Documentation

Easy to get started documentation which is also howto-alike enables users to have their first success with the software. Also, new features need to be tested and documentation sometimes suffers from being written by a developer, not a user.

From my many years of community support in the monitoring ecosystem, I learned how users see things and have been trying to write good documentation ever since. At least 50% of the [Icinga 2 documentation](https://icinga.com/docs/icinga2/latest/) are written by me. Many parts have been rewritten or improved from community feedback.

I'm also a friend of enabling contributors with their first project. Usually I test new things, and try to guide them with a good README introduction, documentation and also project management. I did that in the past e.g. for the [map](https://github.com/nbuchwitz/icingaweb2-module-map/pull/2) and [grafana](https://github.com/Mikesch-mp/icingaweb2-module-grafana/pull/27) modules for Icinga.

I've also started an initiative of documenting [technical design concepts](https://icinga.com/docs/icinga2/latest/doc/19-technical-concepts/). These insights are really appreciated by community members, and I am encouraging other developers to do the same. 

The GitLab website and documentation should become more easier to contribute to. With the nice Web IDE, everyone can do that. When I see a typo, or wrong grammar, I immediately fix this in a [MR](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34491). 

Enable developers to get easily started with CI for their language, including best practices. This e.g. includes ccache for C++. Write howtos and blog posts from the different ranges. Also target small communities, next to new open source projects like Pretix.

I speak Markdown fluently, I prefer it over Google Docs, Confluence, etc. 

<a id="getting-started-trainings"></a>
### Trainings

> @sytses on Training and Learning within GitLab: https://www.youtube.com/watch?v=62j3iysRLd8 

The NETWAYS trainings on GitLab serve a good purpose to create freely available hands-on sessions. Such things could be moved into a workshop at meetups and conferences. Not to enable consultancy/money flow, but to give community members a kick-off and encourage them to use GitLab further.

https://github.com/NETWAYS/gitlab-training
https://github.com/NETWAYS/gitlab-advanced-training 
https://github.com/NETWAYS/workshop-osmc-gitlab

These workshops also allow to explain the enterprise features better, and use subtle marketing here. The "Merge Request Approval" feature is one of the often discussed ones where people typically agree more during a workshop/training than in the online communities.

If we find a good way to also enable GitLab heroes to use these materials in meetups, this would even be better.


<a id="knowledge-evangelism"></a>
## Knowledge and Evangelism

From being a developer, I have become a community supporter and builder. I also do love marketing in the way of sharing my expertise on topics and enabling others to get things going. On social media, I have an active and engaging list of followers who not only follow because of Icinga. Instead, they love tech and leisure activities. 

I'm all in for praising and saying thanks to contributors and team members. Especially developers suffer from "it doesn't work" feedback all the time. 

Lately I've also shared my feedback on GitLab Commit with the events team which was well received by @emily :) 

<a id="knowledge-evangelism-people-media"></a>
### People and Media

I'm a freelance author for the iX magazine from heise, the most popular magazine in technical Germany. An idea would be to have some manuscripts for articles ready on bleeding edge technology.

Friendly people all over the world:

- Icinga (everyone in the community knows me), NETWAYS customers
- DevOpsDays, friendly contact with the founders of DevOps
- Sensu, Zabbix, OpenNMS, Centreon, etc. - take OSMC as conference to connect.
- Foreman/RedHat/Ansible community leaders
- Puppet, CTO & developers
- Elastic, team leads & developer advocates
- Graylog, founder and development team
- InfluxDB, systems engineers, DevRels (also OpenTracing insights)
- Pivotal, open source community builders
- SuSE with their HQ in Nuremberg
- DNS influencers and global registries (nic.at, denic)
- NRENs, academic networks, ISPs, carriers in Europe, Géant WGs (from my past job in Vienna at ZID/ACOnet/VIX)
- CERN, via Icinga
- GitHub, friendly feedback loop and some personal contacts in various positions
- booking.com, Microsoft, IBM, etc. from the DevOps world
- Many more whose names I forgot ;)

<a id="knowledge-evangelism-background"></a>
### Background

With developing Icinga, I always wanted to learn how others use it. In the former data center, now cloud, many tools exist. I started early to receive trainings and knowledge transfer on Puppet, Metrics with Graphite/InfluxDB, etc.

Starting with Icinga in 2009, I never had heard of Docker or any sorts of CI/CD pipeline. Git also was relatively new, everyone used CVS/SVN still. In 2011, we installed Hudson CI, with later moving to Jenkins. The tests where run on the master itself. Since Icinga 2 as agent runs on Windows too, we also learned many things about setting up a build agent with a Visual Studio instance.
The next build server iteration added dedicated build agents, which not only tested the source code and unit tests, but actually generated RPM/DEB packages. I also learned how to create them, I use that on a daily basis. 

The Jenkins config was done via the web interface, but at some point this became overly complicated with 20+ distributions in a build matrix and so on. The XML config backend isn't really tempting. In this regard, the next iteration moved this into Puppet deployments.

Set aside the configuration, the builds for Icinga 2 where the development started in 2012, were done in Docker containers already. The first Docker agent was run in the OpenNebula cloud, with later shifting to CoreOS.

The Docker image creation was hard and not in a good documentation shape. This was rebuilt into better containers too. 

During this period, we've evaluated many other tools such as GitHub Enterprise, JetBrains TeamCity, GoCD and others. From 2018 to 2019 and GitLab being available for Git repositories already, we've made the migration. This not only included dedicated build repositories ("rpm-icinga2") but also [build scripts](https://git.icinga.com/build-docker/scripts) for Docker images and even runners with [autoscaling in OpenStack](https://www.netways.de/blog/2019/10/24/gitlab-ci-runners-with-auto-scaling-on-openstack/). 

Over the years we've created trainings for Git, Jenkins and Docker. This includes best practices from learned Git workflows. Lately we've combined this into GitLab trainings, 2+ years. They cover not learning how to use GitLab, but also follow along best practices with CI/CD and advanced techniques for development teams.

<a id="knowledge-evangelism-evangelism"></a>
### Evangelism

Many tools and technologies I maintain in my stack are self-taught from attending conferences, watching talks and trying them out on my own. Whenever there is a Vagrant/Docker integration available, I keep using this. If not, I create my own demo sandbox. Mostly with the idea in mind that others can find this useful as well. When travelling, I typically read Perry Rhodan (sci-fi) or get a hand into a new book in tools or programming.

The [Icinga Vagrant Boxes](https://github.com/Icinga/icinga-vagrant) are the result of sharing knowledge and enabling others to quickly use the software. Installation isn't important. People keep telling me that they now could easily show the entire feature range to their managers to convince them. 

In my experience, customers in DACH are now (5 years late) in the mood to become agile, use Git and GitLab. If you want to reach banks, car manufacturers, etc. you'll either need a manager catching GitLab, or, yet better, reach the sysadmin and tell them **why** developers need GitLab. 
They also need to learn about containers and Docker in an abstracted way with GitLab CI/CD. Kubernetes is way over the top at first glance, depending on the size of the company. 
Doing inhouse trainings and workshops is something which enables them to motivate their development teams to change from "works somehow with SVN" to seeing the benefits with GitLab. 

> Pushing opinions on bleeding edge tools further can be helped with actual visible appealing demos, or graphics. Or just documentation how to get users started by themselves.

<a id="knowledge-evangelism-regions"></a>
### Regions

Target: Globally.

Idea:

- DeliveryConf 2021, Chicago. We have the chance to create DeliveryConf EU, https://twitter.com/toshywoshy/status/1197538415115407361 

Local regions:

- Germany, Austria: Stronger adoption via local content, German blog posts, talks, etc.
- London/Dublin, met Brian Byrne at GitLabCommit - https://twitter.com/BrianLinuxing/status/1192774606207229952
- Berlin/Frankfurt/Munich/Vienna/Linz/Salzburg, use the contacts gained from Icinga
- EU, DevOpsDays community - stronger focus on technical and cultural talks and ignites

Enable in the developer community from Icinga/NETWAYS into GitLab.

<a id="knowledge-evangelism-knowledge"></a>
### Knowledge

With my long history of developing monitoring tools, I can offer collecting knowledge and blog posts in the Prometheus/Grafana/Loki region. Generally speaking, next to my CV, I really do like to extend my knowledge and learn new things. This not only covers tools and topics but also programming languages.

I'm an expert in building RPM/DEB packages which enable users to quickly start with software, in contrast to self-compile/build.

I also love to meet people and culture all over the world. Sharing knowledge in person is a good thing while having a good time and cheer.



<a id="knowledge-evangelism-community-building"></a>
### Community Building

This shouldn't replace the existing teams, just some ideas on how I can be of help.

- Write Howtos on Discourse & help users - https://forum.gitlab.com/
- Community chats, started that with Icinga https://www.youtube.com/watch?v=7m5TdVfqbcc&t=9s (it already contains references to GitLab, bringing in some experiences which is just subtle marketing)
- Community is built on Discourse, I'm also involved in the greater Discourse community. Make it a great place to stay, and also promote GitLab's Discourse usage stronger on their meta.discourse.org.

#### Community Howtos

Follow https://twitter.com/search?q=gitlab&f=live in TweetDeck.

https://medium.com/@caenderl/gitlab-runner-in-kubernetes-with-minio-cache-875fb283e499

#### Praise 

- Pick cool features, screenshots for social media, etc.
- Spread the love via private accounts
- Instrument others to do the same.

<a id="history"></a>
## History

<a id="history-articles-blogposts"></a>
### Articles & Blogposts

- Heise iX 04/2019: Quo vadis, DevOps? - GitLab CI und CI installieren https://www.heise.de/select/ix/2019/4/1553943972989985
- Heise iX 04/2018: Small things monitoring - kommerzielle und Open-Source-Werkzeuge https://www.heise.de/select/ix/2018/4/1522451271629282
- Heise iX Special 2014: Systemüberwachung mit Icinga 2 https://shop.heise.de/katalog/ix-open-source-administration

- NETWAYS Blog https://www.netways.de/blog/author/mfriedrich/

- GitLab CI Runners with Auto-scaling on OpenStack https://www.netways.de/blog/2019/10/24/gitlab-ci-runners-with-auto-scaling-on-openstack/
- GitLab Commit London Recap https://www.netways.de/blog/2019/10/10/gitlab-commit-london-recap/
- Ich habe einen iX-Artikel für Dich: GitLab, GitLab, GitLab https://www.netways.de/blog/2019/03/21/ich-habe-einen-ix-artikel-fuer-dich-gitlab-gitlab-gitlab/
- A peek into GitLab 11, the Web IDE and Auto DevOps with Kubernetes https://www.netways.de/blog/2018/06/28/a-peek-into-gitlab-11-the-web-ide-and-auto-devops-with-kubernetes/
- Continuous Integration with Golang and GitLab https://www.netways.de/blog/2018/06/07/continuous-integration-with-golang-and-gitlab/
- Releasing our Git and GitLab training as Open Source https://www.netways.de/blog/2018/05/24/releasing-our-git-and-gitlab-training-as-open-source/
- It's magic: git squash https://www.netways.de/blog/2016/11/24/its-magic-git-squash/
- Working with git subtree https://www.netways.de/blog/2016/01/14/working-with-git-subtree/

<a id="history-book-reviews"></a>
### Book reviews

- Icinga 2, Lennart Betz, Thomas Widhalm: https://www.dpunkt.de/buecher/13134/9783864905568-icinga-2.html
- Nagios/Icinga Kochbuch, Timo Kuzca, Ralf Staudemeyer: https://www.amazon.de/Das-Nagios-Icinga-Kochbuch-Timo-Kucza/dp/3868993460

